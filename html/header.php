<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewpoirt" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="index.css" >
  <title> Ajda Savarin </title>
</head>

<body class="all">
  <div class="container">
    <div class="row bottom">
      <div class="col-sm-4"> <a href="https://atmos.uw.edu"><img src="imgs/uw_logo.png" height=80 alt="UW logo" class="rounded mx-auto d-block"></a> </div>
      <div class="col-sm-8"> <a href="https://orca.atmos.washington.edu"><img src="imgs/group_logo.png" height=80 alt="Group logo" class="rounded mx-auto d-block"></a> </div>
    </div> 
    <div class="row" height=400>
      <div class="col-sm-4 vert-center"> <a href="index.php"><img src="imgs/personal_picture.png" alt="Personal picture" class="img-fluid"></a> </div>
      <div class="col-sm-8"> 
        <div class="row">
          <div class="col-sm-12">
            <h6 class="text-center"> Hurricanes and Coupled Atmosphere-Ocean Systems </h6> <br> 
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <h1> Ajda Savarin </h1>
            <p> Graduate student <br> Department of Atmospheric Sciences <br> University of Washington, Seattle </p>
            <hr>
            <p> My research interest is in high-resolution atmosphere-ocean coupled modeling of the Madden-Julian Oscillation with a focus on its passage over the Maritime Continent. </p>
          </div>
        </div> 
      </div>
    </div>     
  </div>












