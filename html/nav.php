

  <nav class="navbar navbar-expand" id="top-of-page">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul id="menu-main-nav" class="navbar-nav nav-fill w-100">
          <li id="menu-item-42" class="nav-item  menu-item-42" ><a href="./index.php" class="nav-link" <?php if ($thisPage == "Bio") echo "id=\"im-here\"" ?>>BIO</a></li>
          <li id="menu-item-963" class="nav-item menu-item menu-item-type-post_type menu-item-object-page menu-item-963"><a href="./research1.php" class="nav-link" <?php if ($thisPage == "Research1") echo "id=\"im-here\"" ?>>RESEARCH</a></li>
          <li id="menu-item-40" class="nav-item menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a href="publications.php" class="nav-link" <?php if ($thisPage == "Publications") echo "id=\"im-here\"" ?>>PUBLICATIONS</a></li>
          <li id="menu-item-42" class="nav-item  menu-item-42"><a href="docs/name_cv.pdf" class="nav-link" target="_blank">CV</a></li>
        </ul>
      </div>
    </div>
  </nav>